﻿using System;


namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] InArray = new int[] { 1, 5, 13, 4, 8, 27, 21, 66, 100 };
            int[] OutArray =  BuublesSort(InArray);
            ReadSolution(OutArray);
            Console.ReadLine();


        }

        /// <summary>
        /// Swapping adjacement elemenys if they are in wrong order.
        /// O(n^2) time(złożonośc kwadratowa)
        /// </summary>
        /// <param name="InArray"></param>
        /// <returns></returns>
        public static int[] BuublesSort(int[] InArray)
        {
            bool swapped;
            int n = InArray.Length;
            for (int i = 0; i < n - 1; i++)
            {
                swapped = false;
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (InArray[j] > InArray[j + 1])
                    {
                        int tempValue = InArray[j];
                        InArray[j] = InArray[j + 1];
                        InArray[j + 1] = tempValue;
                        swapped = true;
                    }
                }
                if (swapped == false)
                    break;
            }
            
            return InArray;
        }

        public static void ReadSolution(int [] ArrayToDisplay)
        {
            foreach (var element in ArrayToDisplay)
            {
                Console.WriteLine(element);
            }
        }


       
    }
}
